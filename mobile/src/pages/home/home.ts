import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RdvPage } from '../rdv/rdv';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  takeRdv() {
    this.navCtrl.push(RdvPage);
  }

}
