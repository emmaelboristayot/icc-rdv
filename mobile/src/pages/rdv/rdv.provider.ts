import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

@Injectable()
export class RdvProvider {

  constructor(public http: HttpClient) {
  }

  public takeRdv = (rdv) => this.http.post('http://lanormandiepourchrist.com/borne-icc/back/public/index.php/api/new', rdv) as Observable<any>

}
