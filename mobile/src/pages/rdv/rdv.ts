import { HomePage } from "./../home/home";
import { ModalSendingComponent } from "./modal-sending/modal-sending";
import { Component } from "@angular/core";
import { Platform, NavController, ModalController, LoadingController } from "ionic-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { RdvProvider } from "./rdv.provider";

@Component({
  selector: "rdv",
  templateUrl: "rdv.html"
})
export class RdvPage {
  rdv: FormGroup = this.fb.group({
    genre: ["", [Validators.required]],
    nom: ["", [Validators.required]],
    prenom: ["", [Validators.required]],
    telephone: ["", [Validators.required, Validators.pattern("[0-9]+")]],
    email: ["", [Validators.required, Validators.email]],
    sujet: ["", [Validators.required]],
    commentaire: ["", [Validators.required]]
  });

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    private fb: FormBuilder,
    private rdvFormProvider: RdvProvider,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController
  ) {}

  ionViewDidLoad() {
  }

  onSubmit() {
    //Loader
    const loader = this.loadingCtrl.create({
      content: 'Envoi en cours...',
    });
    loader.present()

    this.rdvFormProvider.takeRdv(this.rdv.value).subscribe(
      data => {
        loader.dismiss();

        const modal = this.modalCtrl.create(
          ModalSendingComponent,
          { response: data },
          { enableBackdropDismiss: false }
        );
        modal.present();
      },
      error => {
        loader.dismiss();

        const modal = this.modalCtrl.create(
          ModalSendingComponent,
          { response: error },
          { enableBackdropDismiss: false }
        );
        modal.present();
      }
    );
  }

  backToHome() {
    event.preventDefault();
    this.navCtrl.push(HomePage);
  }
}
