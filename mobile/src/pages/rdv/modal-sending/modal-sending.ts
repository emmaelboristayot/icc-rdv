import { Component } from "@angular/core";
import { NavController, NavParams, ViewController } from "ionic-angular";
import { HomePage } from "../../home/home";

@Component({
  selector: "page-modal-sending",
  templateUrl: "modal-sending.html"
})
export class ModalSendingComponent {
  public readonly response: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {
    this.response = this.navParams.get("response");
  }

  ionViewDidLoad() {
    setTimeout(() => {
      this.viewCtrl.dismiss();
      this.navCtrl.push(HomePage);
    }, 10000);
  }

  goToHomePage() {
    this.navCtrl.push(HomePage);
  }
}
