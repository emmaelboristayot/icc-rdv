import Vue from 'vue'
import Vuex from 'vuex'

import auth from './auth'
import appointment from './appointment'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    appointment,
    auth
  }
})
