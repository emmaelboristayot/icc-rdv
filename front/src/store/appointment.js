const state = {
  appointments: [],
  appointmentSelected: {}
};

const mutations = {
  gets(state, { appointments }) {
    // mutate state
    state.appointments = appointments;
  },
  show(state, { appointmentSelected }) {
    // mutate state
    state.appointmentSelected = appointmentSelected;
  }
};

const getters = {
  appointments (state) {
    return state.appointments
  },
  appointmentSelected (state) {
    return state.appointmentSelected
  }
};

const actions = {};

export default {
  state,
  mutations,
  getters,
  actions
};
