import JwtDecode from 'jwt-decode'

export default class User {
  static from (token) {
    try {
      let obj = JwtDecode(token)
      return new User(obj)
    } catch (_) {
      return null
    }
  }

  constructor ({ exp, iat, roles, username }) {
    this.exp = exp
    this.iat = iat
    this.roles = roles
    this.username = username
  }

  get isAdmin () {
    return this.admin
  }
}
